package com.tdtri.underlinetextview

import android.content.Context
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

class UnderlineTextView : AppCompatTextView {
    constructor(context: Context?) : super(context!!) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context!!, attrs, defStyle) {
        init()
    }

    private fun init() {
        underLineText(null)
    }

    private fun underLineText(value : String?) {
        val content = SpannableString(value ?: text) // if value == null, will use text from xml.
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        text = content
    }

}